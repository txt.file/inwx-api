#!/usr/bin/env python3
"""
Check availability and annual cost of each tld for a given domain.
"""

__author__ = "Vieno Hakkerinen"
__version__ = "0.0.0"
__license__ = "MIT"

import json
import sys

from INWX.Domrobot import ApiClient

username = 'user'
password = 'passphrase'
domain = 'example' # domain without tld
region = 'none' # sadly I have not found a list of valid regions. You need to figure them out yourself. 😞

# domain can be given as command line parameter
if len(sys.argv) >= 2 and '' != str(sys.argv[1]):
    domain = sys.argv[1]

# region can be given as command line parameter
if len(sys.argv) >= 3 and '' != str(sys.argv[2]):
    region = sys.argv[2]

api_client = ApiClient(api_url=ApiClient.API_LIVE_URL, debug_mode=False)

login_result = api_client.login(username, password)

if login_result['code'] == 1000:
    domain_check_result = api_client.call_api(api_method='domain.check', method_params={'domain': domain, 'wide': 2, 'region': region})

    if domain_check_result['code'] == 1000:
        for checked_domain in domain_check_result['resData']['domain']:
            if checked_domain['avail']:
                if '.' in checked_domain['tld']:
                    continue
                prices = api_client.call_api(api_method='domain.getprices', method_params={'tld': checked_domain['tld'], 'vatCC': 'DE'})
                if prices['code'] != 1000:
                    continue
                prices = prices['resData']['price'][0]

                output = checked_domain['domain'] + ','
                output += str(prices['createPrice']) + ',' + str(prices['renewalPrice']) + ','
                if 'promo' in prices:
                    output += str(prices['promo']['createPrice']) 
                print(output)
    else:
        raise Exception('Api error while checking domain status. Code: ' + str(domain_check_result['code'])
                        + '  Message: ' + domain_check_result['msg'])
    api_client.logout()
else:
    raise Exception('Api login error. Code: ' + str(login_result['code']) + '  Message: ' + login_result['msg'])
